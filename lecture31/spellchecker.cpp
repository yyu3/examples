// spellchecker.cpp

#include "compressed_set.hpp"

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

// Main Execution

int main(int argc, char *argv[]) {
    compressed_set dict(235886, 1e-2);
    string line;

    // Load dictionary
    ifstream dictfile("/usr/share/dict/words");
    while (getline(dictfile, line)) {
        dict.insert(line);
    }

    while (getline(cin, line)) {
        cout << line << " is " << (dict.search(line) ? "GOOD" : "BAD") << endl;
    }

    return EXIT_SUCCESS;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
