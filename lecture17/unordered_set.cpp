#include <iostream>

#include <unordered_set>

using namespace std;

int main(int argc, char *argv[]) {
    unordered_set<int> s;

    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return 1;
    }

    int nitems = atoi(argv[1]);

    for (int i = 0; i < nitems; i++) {
    	s.insert(i);
    }

    for (int i = 0; i < nitems; i++) {
    	s.find(i);
    }

    return 0;
}
