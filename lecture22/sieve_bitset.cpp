// sieve_array.cpp

#include <cmath>
#include <cstdint>
#include <iostream>

const size_t N = 1<<20;
const size_t C = N / sizeof(uint64_t);

using namespace std;

int main(int argc, char *argv[]) {
    uint64_t bitset[C];

    // Initialize numbers
    for (size_t c = 0; c < C; c++) {
        bitset[c] = ~0;
    }

    // Sieve
    for (size_t i = 2; i < (size_t)(sqrt(N)); i++) {
        size_t c = i / sizeof(uint64_t);
        size_t b = i % sizeof(uint64_t);

        if (bitset[c] & 1<<b) {
            for (size_t p = i*i; p < N; p += i) {
                c = p / sizeof(uint64_t);
                b = p % sizeof(uint64_t);
                bitset[c] &= ~(1<<b);
            }
        }
    }
    
    // Output
    size_t count = 0;
    for (size_t c = 0; c < C; c++) {
        for (size_t b = 0; b < sizeof(uint64_t); b++) {
            if (!c && !b)
                continue;

            if (bitset[c] & 1<<b) {
                count++;
    	        //cout << c*sizeof(uint64_t) + b << endl;
            }
        }
    }
    cout << count << endl;

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
